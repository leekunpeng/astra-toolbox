#include "astra/CompositeGeometryManager.h"
#include "cpp/creators.hpp"
#include "ctheader.h"

#include <iostream>
#include <omp.h>
#include <sys/time.h>
#include <string.h>

using namespace std;
using namespace astra;

enum operation {
    OP_PRODUCT,
    OP_SUM,
    OP_SINO,
    OP_SINO_NORM,
    OP_NORM,
    OP_MASK_NORM
};

inline static size_t dataSize(CFloat32Data3DMemory *data) {
    return (size_t)data->getWidth() * data->getHeight() * data->getDepth();
}

/* NOTICE: adjust size of buffers according to disk transfer and processor speed */
#define BUFSIZE 0x4000000L  /* 256 MiB worth of floats */
static void bufferedOpGZ(const char *filename, CFloat32Data3DMemory *data, enum operation op, double *norm_o) {
    const static char opname[6][10] = {"PRODUCT","SUM","SINO","SINO_NORM","NORM","MASK_NORM"};
    float32 *buf,*dat,val,*p;
    struct timeval tp;
    size_t ix,n,read;
    double ti,tf;
    double norm;
    int i,nbuf;
    gzFile zfp;

    fprintf(stderr,"BUFFERED %s ... ",opname[op]);
    gettimeofday(&tp,NULL); ti = tp.tv_sec + 1E-6 * tp.tv_usec;
    zfp = gzopen(filename,"rb");
    gzbuffer(zfp,BUFSIZE);
    ASTRA_ASSERT(zfp != NULL);
    dat = data->getData();
    buf = new float32[BUFSIZE];
    nbuf = dataSize(data) / BUFSIZE;
    if(dataSize(data) % BUFSIZE > 0) nbuf++;
    if(op == OP_SINO_NORM || op == OP_NORM || op == OP_MASK_NORM) *norm_o = 0.0;
    read = 0;
    for(i=0;i<nbuf;i++) {
        n = gzfread(buf,sizeof(float32),BUFSIZE,zfp);
        ASTRA_ASSERT(n == BUFSIZE || gzeof(zfp));
        switch(op) {
            case OP_PRODUCT:
#pragma omp parallel for private(ix)
                for(ix=0;ix<n;ix++)
                    dat[ix + read] *= buf[ix];
                break;
            case OP_SUM:
#pragma omp parallel for private(ix)
                for(ix=0;ix<n;ix++)
                    dat[ix + read] += buf[ix];
                break;
            case OP_SINO:
#pragma omp parallel for private(ix)
                for(ix=0;ix<n;ix++)
                    dat[ix + read] = buf[ix] - dat[ix + read];
                break;
            case OP_SINO_NORM:
                norm = 0.0;
#pragma omp parallel for private(ix,p) reduction(+:norm)
                for(ix=0;ix<n;ix++) {
                    p = &dat[ix + read];
                    *p = buf[ix] - *p;
                    norm += *p * *p;
                }
                *norm_o += norm;
                break;
            case OP_NORM:
                norm = 0.0;
#pragma omp parallel for private(ix,val) reduction(+:norm)
                for(ix=0;ix<n;ix++) {
                    val = buf[ix] - dat[ix + read];
                    norm += val * val;
                }
                *norm_o += norm;
                break;
            case OP_MASK_NORM:
                norm = 0.0;
#pragma omp parallel for private(ix,p) reduction(+:norm)
                for(ix=0;ix<n;ix++) {
                    p = &dat[ix + read];
                    if(buf[ix] == 0.0) *p = 0.0;
                    norm += *p * *p;
                }
                *norm_o += norm;
                break;
        }
        read += n;
        //fprintf(stderr,"%s BUFFER %d/%d\n",opname,i+1,nbuf);
    }
    delete[] buf;
    gzclose(zfp);
    gettimeofday(&tp,NULL); tf = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"DONE IN %g MINUTES\n",(tf-ti)/60.0);
}

static void rotate_z(double angle, double x, double y, double z, double *x_o, double *y_o, double *z_o)
{
    *x_o = cos(angle) * x - sin(angle) * y;
    *y_o = sin(angle) * x + cos(angle) * y;
    *z_o = z;
}

// nx * ny * nz are the volume dimensions and (x0,y0,z0) is the volume center
CVolumeGeometry3D* createVolGeom(double voxel_size, int nx, int ny, int nz, double x0, double y0, double z0) {
    const double xmin = x0 - (voxel_size * nx) * 0.5;
    const double xmax = x0 + (voxel_size * nx) * 0.5;
    const double ymin = y0 - (voxel_size * ny) * 0.5;
    const double ymax = y0 + (voxel_size * ny) * 0.5;
    const double zmin = z0 - (voxel_size * nz) * 0.5;
    const double zmax = z0 + (voxel_size * nz) * 0.5;

    return create_vol_geom_3d(nx,ny,nz,xmin,xmax,ymin,ymax,zmin,zmax);
}

CConeVecProjectionGeometry3D* createConeProjGeom(const CT_FILE_HEADER *cthdr) {
    // sinogram dimensions
    const int nproj = cthdr->NProy; // angular resolution
    const int det_perp = cthdr->Pixels_X; // u resolution
    const int det_axial = cthdr->Pixels_Z; // v resolution
    const double pix = cthdr->PixelSize; // in mm

    // geometric calibration parameters
    const double R = cthdr->ctgeom.R; // in mm
    const double D = cthdr->ctgeom.focal; // in mm
    const double u_0 = cthdr->ctgeom.u_0; // in mm
    const double v_0 = cthdr->ctgeom.v_0; // in mm
    const double twist = cthdr->ctgeom.twist; // in degrees
    const double slant = cthdr->ctgeom.slant; // in degrees
    const double tilt  = cthdr->ctgeom.tilt;  // in degrees

    // From CT Calibration Bruker presentation
    // Tilt (rot. angle around x)
    // Slant (z)
    // Twist (y)
    // Where u <-> x, v <-> y, w <-> z
    // However, eta = twist & theta = slant (VERIFIED)
    // angles in radians
    const double eta   = twist * M_PI / 180.0; // in-plane angle
    const double theta = slant * M_PI / 180.0; // polar angle
    const double phi   = tilt  * M_PI / 180.0; // azimuthal angle

    // return value
    CConeVecProjectionGeometry3D *proj_geom;

    // workspace
    double e_w[3],e_u[3],e_v[3],alpha[3],beta[3];
    double src[3],det[3],pix_u[3],pix_v[3];
    float32 *angles;
    double *vectors;
    double gamma;
    int i;

    /* ===================================
       CONE BEAM GEOMETRY (Noo et al. '00)
       =================================== */

    // normal to detector plane
    e_w[0] = cos(theta) * cos(phi);
    e_w[1] = cos(theta) * sin(phi);
    e_w[2] = sin(theta);

    // x orthogonal direction from e_w
    alpha[0] = -sin(phi);
    alpha[1] =  cos(phi);
    alpha[2] =  0;

    // y orthogonal direction from e_w
    beta[0] = -sin(theta) * cos(phi);
    beta[1] = -sin(theta) * sin(phi);
    beta[2] =  cos(theta);

    // x direction along detector plane (including in-plane rotation)
    e_u[0] = cos(eta) * alpha[0] + sin(eta) * beta[0];
    e_u[1] = cos(eta) * alpha[1] + sin(eta) * beta[1];
    e_u[2] = cos(eta) * alpha[2] + sin(eta) * beta[2];

    // y direction along detector plane (including in-plane rotation)
    e_v[0] = cos(eta) * beta[0] - sin(eta) * alpha[0];
    e_v[1] = cos(eta) * beta[1] - sin(eta) * alpha[1];
    e_v[2] = cos(eta) * beta[2] - sin(eta) * alpha[2];

    // geometry at projection angle 0
    // cone vertex (source)
    src[0] = R;
    src[1] = 0;
    src[2] = 0;
    // vector from det pixel (0,0) to (1,0)
    // pix_u = pix * e_u
    pix_u[0] = pix * e_u[0];
    pix_u[1] = pix * e_u[1];
    pix_u[2] = pix * e_u[2];
    // vector from det pixel (0,0) to (0,1)
    // pix_v = pix * e_v
    pix_v[0] = pix * e_v[0];
    pix_v[1] = pix * e_v[1];
    pix_v[2] = pix * e_v[2];
    // center of detector (at a distance D from source, displaced by u_0/2 and v_0/2)
    // det = src    - D * e_w    + 0.5 * u_0 * e_u    + 0.5 * v_0 * e_v
    det[0] = src[0] - D * e_w[0] + 0.5 * u_0 * e_u[0] + 0.5 * v_0 * e_v[0];
    det[1] = src[1] - D * e_w[1] + 0.5 * u_0 * e_u[1] + 0.5 * v_0 * e_v[1];
    det[2] = src[2] - D * e_w[2] + 0.5 * u_0 * e_u[2] + 0.5 * v_0 * e_v[2];

    // rotate x -> -y (seems to work only this way!)
    gamma = -M_PI_2;
    rotate_z(gamma,src[0],src[1],src[2],&src[0],&src[1],&src[2]);
    rotate_z(gamma,det[0],det[1],det[2],&det[0],&det[1],&det[2]);
    rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&pix_u[0],&pix_u[1],&pix_u[2]);
    rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&pix_v[0],&pix_v[1],&pix_v[2]);

    // rotate by projection angle around Z
    angles = create_angles(0,2*M_PI,nproj);
    vectors = new double[12*nproj];
    for(i=0;i<nproj;i++) {
        gamma = angles[i];
        rotate_z(gamma,src[0],src[1],src[2],&vectors[12*i+0],&vectors[12*i+1],&vectors[12*i+2]);
        rotate_z(gamma,det[0],det[1],det[2],&vectors[12*i+3],&vectors[12*i+4],&vectors[12*i+5]);
        rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&vectors[12*i+6],&vectors[12*i+7],&vectors[12*i+8]);
        rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&vectors[12*i+9],&vectors[12*i+10],&vectors[12*i+11]);
    }
    delete[] angles;

    proj_geom = create_proj_geom_3d_cone_vec(det_axial,det_perp,nproj,vectors);
    delete[] vectors;

    return proj_geom;
}

int main (int argc, char *argv[])
{
    CCudaProjector3D *proy;
    CVolumeGeometry3D *vol_geom;
    CConeVecProjectionGeometry3D *proj_geom;
    CFloat32ProjectionData3DMemory *projData;
    CFloat32VolumeData3DMemory *volumeData,*tmpData;
    CCompositeGeometryManager *cgeomgr;
    float32 *ptmp;
    const float32 *ctmp;
    struct timeval tp;
    double t0,t1,t2,dt,tx;
    size_t ix;
    FILE *fp;

    CT_FILE_HEADER cthdr;
    unsigned int xres,yres,zres;
    double voxel_size; // in mm

    const char *file_cthdr,*file_sino,*path_temp,*str_iter,*str_geom;
    char *file_out,*ext;

    if(argc != 7) {
        fprintf(stderr,"[ERROR] Please supply CT header, sinogram, tmp path, geometry, iterations and output filenames\n");
        exit(EXIT_FAILURE);
    }

    file_cthdr = argv[1];
    file_sino  = argv[2];
    path_temp  = argv[3];
    str_geom   = argv[4];
    str_iter   = argv[5];
    file_out   = strdup(argv[6]);

    // load CT header data
    if(ctheader_read(file_cthdr,&cthdr)) {
        fprintf(stderr,"[ERROR] Cannot read CT header: %s\n",file_cthdr);
        exit(EXIT_FAILURE);
    }

    // get dimensions from str_geom
    sscanf(str_geom,"%ux%ux%u@%lf",&xres,&yres,&zres,&voxel_size);

    // create volume geometry                         FOV center (mm)
    vol_geom = createVolGeom(voxel_size,xres,yres,zres,0.0,0.0,+2.0);

    // create cone-beam projection geometry
    proj_geom = createConeProjGeom(&cthdr);

    // create CUDA 3D projector
    proy = create_projector_3d_cuda(proj_geom,vol_geom);

    // create composite geometry manager
    cgeomgr = new CCompositeGeometryManager();

    /* =============
       EMULATE SIRT!
       ============= */

    // Memory usage is 3 x (volume data + projection data), once for input data,
    // another for output data, the third for precomputed weights

    // max iterations
    unsigned int maxiter = atoi(str_iter);

    char path_lW[1024],path_pW[1024];
    const float32 relaxation = 1.99f; // epsilon = 0.005 (Gregor & Benson '08)
    bool useMinConstraint = true;
    const float32 MinConstraint = 0.0;
    bool useMaxConstraint = true;
    const float32 MaxConstraint = 1.0;
    const int saveiter = 25; // save every this iterations
    const int pauseiter = 50; // pause every this iterations
    const int pause = 300; // pause length in seconds
    double norm,inorm = 0.0;
    unsigned int resume = 0;
    unsigned int iter;

    // initial time  (seconds)
    gettimeofday(&tp,NULL); t0 = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"Sinogram: %s\n",file_sino);
    
    // START alloc
    projData = create_data_3d_sino(proj_geom);
    tmpData = create_data_3d_vol(vol_geom);
    // END alloc

    // START precompute weights
    snprintf(path_lW,sizeof(path_lW),"%s/%s",path_temp,"lineWeight.img");
    fp = fopen(path_lW,"r");
    if(fp != NULL) {
        fclose(fp);
        fprintf(stderr,"Line weights found: %s\n",path_lW);
    } else { /* non-existing */
        tmpData->setData(1.0f);
        fprintf(stderr,"FP start ... ");
        cgeomgr->doFP(proy,tmpData,projData);
        fprintf(stderr,"FP done\n");
        ptmp = projData->getData();
#pragma omp parallel for private(ix)
        for(ix=0;ix<dataSize(projData);ix++)
            ptmp[ix] = ptmp[ix] > 0.000001f ? 1 / ptmp[ix] : 0.0f;
        save_data_3d(path_lW,projData);
        fprintf(stderr,"Line weights precomputed\n");
    }

    snprintf(path_pW,sizeof(path_pW),"%s/%s",path_temp,"pixelWeight.img");
    fp = fopen(path_pW,"r");
    if(fp != NULL) {
        fclose(fp);
        fprintf(stderr,"Pixel weights found: %s\n",path_pW);
    } else { /* non-existing */
        projData->setData(1.0f);
        fprintf(stderr,"BP start ... ");
        cgeomgr->doBP(proy,tmpData,projData);
        fprintf(stderr,"BP done\n");
        ptmp = tmpData->getData();
#pragma omp parallel for private(ix)
        for(ix=0;ix<dataSize(tmpData);ix++)
            ptmp[ix] = (ptmp[ix] > 0.000001f ? 1 / ptmp[ix] : 0.0f) * relaxation;
        save_data_3d(path_pW,tmpData);
        fprintf(stderr,"Pixel weights precomputed\n");
    }
    // END precompute weights

    // START init
    volumeData = create_data_3d_vol(vol_geom,0.0f);
    delete proj_geom;
    delete vol_geom;

    // check for output file
    fp = fopen(file_out,"r");
    if(fp != NULL) {
        fclose(fp);
        resume = 1;
        fprintf(stderr,"Output file found: %s\n",file_out);
    }

    if(resume) // load intermediate result
        volumeData = load_data_3d_vol(file_out,volumeData);
    else // load sinogram
        projData = load_data_3d_sino(file_sino,projData);
    // END init

    // time at start of iterations (seconds)
    gettimeofday(&tp,NULL); t1 = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"Iteration started\n");

    if(resume) fprintf(stderr,"RESUMING...\n");

    // START iterate
    for(iter = 0; iter < maxiter; iter++) {
        fprintf(stderr,"Iteration %d/%d\n",iter+1,maxiter);
        
        if(iter > 0 || resume) { // subsequent iterations
            // forward-project
            fprintf(stderr,"FP start ... ");
            gettimeofday(&tp,NULL); tx = tp.tv_sec + 1E-6 * tp.tv_usec;
            cgeomgr->doFP(proy,volumeData,projData);
            gettimeofday(&tp,NULL);
            fprintf(stderr,"done in %g minutes\n",((tp.tv_sec+1E-6*tp.tv_usec)-tx)/60.0);

            // update sinogram and compute norm
            bufferedOpGZ(file_sino,projData,OP_SINO_NORM,&norm);
            fprintf(stderr,"Norm of difference: %g\n",norm);
        } else { // first iteration
            // compute initial norm
            ctmp = projData->getDataConst();
            inorm = 0.0;
#pragma omp parallel for private(ix) reduction(+:inorm)
            for(ix=0;ix<dataSize(projData);ix++)
                inorm += ctmp[ix] * ctmp[ix];
            inorm = sqrt(inorm);
        }

        // apply line weights
        bufferedOpGZ(path_lW,projData,OP_PRODUCT,NULL);

        // back-project
        fprintf(stderr,"BP start ... ");
        gettimeofday(&tp,NULL); tx = tp.tv_sec + 1E-6 * tp.tv_usec;
        cgeomgr->doBP(proy,tmpData,projData);
        gettimeofday(&tp,NULL);
        fprintf(stderr,"done in %g minutes\n",((tp.tv_sec+1E-6*tp.tv_usec)-tx)/60.0);

        // apply pixel weights
        bufferedOpGZ(path_pW,tmpData,OP_PRODUCT,NULL);

        // sum with volume data
        ptmp = volumeData->getData();
        ctmp = tmpData->getDataConst();
#pragma omp parallel for private(ix)
        for(ix=0;ix<dataSize(volumeData);ix++)
            ptmp[ix] += ctmp[ix];
        
        // clamp min
        if(useMinConstraint) {
            ptmp = volumeData->getData();
#pragma omp parallel for private(ix)
            for(ix=0;ix<dataSize(volumeData);ix++)
                if(ptmp[ix] < MinConstraint)
                    ptmp[ix] = MinConstraint;
        }

        // clamp max
        if(useMaxConstraint) {
            ptmp = volumeData->getData();
#pragma omp parallel for private(ix)
            for(ix=0;ix<dataSize(volumeData);ix++)
                if(ptmp[ix] > MaxConstraint)
                    ptmp[ix] = MaxConstraint;
        }

        // save intermediate result
        if((iter + 1) % saveiter == 0) {
            save_data_3d(file_out,volumeData);
            fprintf(stderr,"Saved intermediate result as %s\n",file_out);
        }

        // idle time to cool GPU
        if(iter + 1 < maxiter && (iter + 1) % pauseiter == 0) {
            fprintf(stderr,"%d of %d (max) iterations: idling %ds to cool GPU... ",iter+1,maxiter,pause);
            sleep(pause);
            fprintf(stderr,"resuming execution\n");
        }
    }
    delete tmpData;

    // final time (seconds)
    gettimeofday(&tp,NULL); t2 = tp.tv_sec + 1E-6 * tp.tv_usec;
    fprintf(stderr,"Iteration ended\n");

    // compute final norm
    fprintf(stderr,"Computing final norm\n");
    fprintf(stderr,"FP start ... ");
    cgeomgr->doFP(proy,volumeData,projData);
    fprintf(stderr,"FP done\n");

    bufferedOpGZ(file_sino,projData,OP_NORM,&norm);
    norm = sqrt(norm);

    delete projData;
    delete cgeomgr;
    delete proy;
    
    // save data
    save_data_3d(file_out,volumeData);
    delete volumeData;

    // change file extension from .img to .log
    ext = strstr(file_out,".img");
    *++ext = 'l'; *++ext = 'o'; *++ext = 'g';

    // statistics
    fp = fopen(file_out,"w");
    fprintf(fp,"Sinogram: %s\n",file_sino);
    fprintf(fp,"Iterations: %d\n",iter);
    fprintf(fp,"Initial norm of difference: %g\n",inorm);
    fprintf(fp,"Final norm of difference: %g\n",norm);
    dt = t2 - t0;
    if(dt < 60.0)
        fprintf(fp,"Total reconstruction time: %gs\n",dt);
    else if(dt < 3600.0)
        fprintf(fp,"Total reconstruction time: %gm\n",dt/60.0);
    else
        fprintf(fp,"Total reconstruction time: %gh\n",dt/3600.0);
    dt = (t2 - t1) / iter;
    if(dt < 60.0)
        fprintf(fp,"Mean time per iteration: %gs\n",dt);
    else
        fprintf(fp,"Mean time per iteration: %gm\n",dt/60.0);
    fclose(fp);

    return 0;
}
