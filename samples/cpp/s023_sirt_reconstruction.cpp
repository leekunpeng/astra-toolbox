#include "astra/CompositeGeometryManager.h"
#include "cpp/creators.hpp"
#include "ctheader.h"

#include <sys/time.h>
#include <string.h>

#ifdef _OPENMP
#include <omp.h>
#endif

using namespace std;
using namespace astra;

inline static size_t dataSize(CFloat32Data3DMemory *data) {
    return (size_t)data->getWidth() * data->getHeight() * data->getDepth();
}

static void rotate_z(double angle, double x, double y, double z, double *x_o, double *y_o, double *z_o)
{
    *x_o = cos(angle) * x - sin(angle) * y;
    *y_o = sin(angle) * x + cos(angle) * y;
    *z_o = z;
}

// nx * ny * nz are the volume dimensions and (x0,y0,z0) is the volume center
CVolumeGeometry3D* createVolGeom(double voxel_size, unsigned int nx, unsigned int ny, unsigned int nz, double x0, double y0, double z0) {
    const double xmin = x0 - (voxel_size * nx) * 0.5;
    const double xmax = x0 + (voxel_size * nx) * 0.5;
    const double ymin = y0 - (voxel_size * ny) * 0.5;
    const double ymax = y0 + (voxel_size * ny) * 0.5;
    const double zmin = z0 - (voxel_size * nz) * 0.5;
    const double zmax = z0 + (voxel_size * nz) * 0.5;

    return create_vol_geom_3d(nx,ny,nz,xmin,xmax,ymin,ymax,zmin,zmax);
}

CConeVecProjectionGeometry3D* createConeProjGeom(const CT_FILE_HEADER *cthdr) {
    // sinogram dimensions
    const int nproj = cthdr->NProy; // angular resolution
    const int det_perp = cthdr->Pixels_X; // u resolution
    const int det_axial = cthdr->Pixels_Z; // v resolution
    const double pix = cthdr->PixelSize; // in mm

    // geometric calibration parameters
    const double R = cthdr->ctgeom.R; // in mm
    const double D = cthdr->ctgeom.focal; // in mm
    const double u_0 = cthdr->ctgeom.u_0; // in mm
    const double v_0 = cthdr->ctgeom.v_0; // in mm
    const double twist = cthdr->ctgeom.twist; // in degrees
    const double slant = cthdr->ctgeom.slant; // in degrees
    const double tilt  = cthdr->ctgeom.tilt;  // in degrees

    // From CT Calibration Bruker presentation
    // Tilt (rot. angle around x)
    // Slant (z)
    // Twist (y)
    // Where u <-> x, v <-> y, w <-> z
    // However, eta = twist & theta = slant (VERIFIED)
    // angles in radians
    const double eta   = twist * M_PI / 180.0; // in-plane angle
    const double theta = slant * M_PI / 180.0; // polar angle
    const double phi   = tilt  * M_PI / 180.0; // azimuthal angle

    // return value
    CConeVecProjectionGeometry3D *proj_geom;

    // workspace
    double e_w[3],e_u[3],e_v[3],alpha[3],beta[3];
    double src[3],det[3],pix_u[3],pix_v[3];
    float32 *angles;
    double *vectors;
    double gamma;
    int i;

    /* ===================================
       CONE BEAM GEOMETRY (Noo et al. '00)
       =================================== */

    // normal to detector plane
    e_w[0] = cos(theta) * cos(phi);
    e_w[1] = cos(theta) * sin(phi);
    e_w[2] = sin(theta);

    // x orthogonal direction from e_w
    alpha[0] = -sin(phi);
    alpha[1] =  cos(phi);
    alpha[2] =  0;

    // y orthogonal direction from e_w
    beta[0] = -sin(theta) * cos(phi);
    beta[1] = -sin(theta) * sin(phi);
    beta[2] =  cos(theta);

    // x direction along detector plane (including in-plane rotation)
    e_u[0] = cos(eta) * alpha[0] + sin(eta) * beta[0];
    e_u[1] = cos(eta) * alpha[1] + sin(eta) * beta[1];
    e_u[2] = cos(eta) * alpha[2] + sin(eta) * beta[2];

    // y direction along detector plane (including in-plane rotation)
    e_v[0] = cos(eta) * beta[0] - sin(eta) * alpha[0];
    e_v[1] = cos(eta) * beta[1] - sin(eta) * alpha[1];
    e_v[2] = cos(eta) * beta[2] - sin(eta) * alpha[2];

    // geometry at projection angle 0
    // cone vertex (source)
    src[0] = R;
    src[1] = 0;
    src[2] = 0;
    // vector from det pixel (0,0) to (1,0)
    // pix_u = pix * e_u
    pix_u[0] = pix * e_u[0];
    pix_u[1] = pix * e_u[1];
    pix_u[2] = pix * e_u[2];
    // vector from det pixel (0,0) to (0,1)
    // pix_v = pix * e_v
    pix_v[0] = pix * e_v[0];
    pix_v[1] = pix * e_v[1];
    pix_v[2] = pix * e_v[2];
    // center of detector (at a distance D from source, displaced by u_0/2 and v_0/2)
    // det = src    - D * e_w    + 0.5 * u_0 * e_u    + 0.5 * v_0 * e_v
    det[0] = src[0] - D * e_w[0] + 0.5 * u_0 * e_u[0] + 0.5 * v_0 * e_v[0];
    det[1] = src[1] - D * e_w[1] + 0.5 * u_0 * e_u[1] + 0.5 * v_0 * e_v[1];
    det[2] = src[2] - D * e_w[2] + 0.5 * u_0 * e_u[2] + 0.5 * v_0 * e_v[2];

    // rotate x -> -y (seems to work only this way!)
    gamma = -M_PI_2;
    rotate_z(gamma,src[0],src[1],src[2],&src[0],&src[1],&src[2]);
    rotate_z(gamma,det[0],det[1],det[2],&det[0],&det[1],&det[2]);
    rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&pix_u[0],&pix_u[1],&pix_u[2]);
    rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&pix_v[0],&pix_v[1],&pix_v[2]);

    // rotate by projection angle around Z
    angles = create_angles(0,2*M_PI,nproj);
    vectors = new double[12*nproj];
    for(i=0;i<nproj;i++) {
        gamma = angles[i];
        rotate_z(gamma,src[0],src[1],src[2],&vectors[12*i+0],&vectors[12*i+1],&vectors[12*i+2]);
        rotate_z(gamma,det[0],det[1],det[2],&vectors[12*i+3],&vectors[12*i+4],&vectors[12*i+5]);
        rotate_z(gamma,pix_u[0],pix_u[1],pix_u[2],&vectors[12*i+6],&vectors[12*i+7],&vectors[12*i+8]);
        rotate_z(gamma,pix_v[0],pix_v[1],pix_v[2],&vectors[12*i+9],&vectors[12*i+10],&vectors[12*i+11]);
    }
    delete[] angles;

    proj_geom = create_proj_geom_3d_cone_vec(det_axial,det_perp,nproj,vectors);
    delete[] vectors;

    return proj_geom;
}

float* astra_sirt_reconstruction(const CT_FILE_HEADER *cthdr, const float *sino_data, const char *str_geom, const unsigned int iter, const bool clampMin, const float min, const bool clampMax, const float max, const bool pause, const bool debug, size_t *size_out) {
    unsigned int xres,yres,zres;
    char xoff_s,yoff_s,zoff_s;
    double xoff,yoff,zoff; //in mm
    double voxel_size; //in mm

    { // get dimensions from str_geom
        char *end;
        xres = strtoul(str_geom,&end,10);
        if(*end != 'x') return NULL;
        str_geom = end + 1;
        yres = strtoul(str_geom,&end,10);
        if(*end != 'x') return NULL;
        str_geom = end + 1;
        zres = strtoul(str_geom,&end,10);
        if(*end != '@') return NULL;
        str_geom = end;
        if(sscanf(str_geom,"@%lf%1[+-]%lf%1[+-]%lf%1[+-]%lf",
                    &voxel_size,&xoff_s,&xoff,&yoff_s,&yoff,&zoff_s,&zoff) != 7)
            return NULL;
    }

    // remove offset signs
    xoff = fabs(xoff);
    yoff = fabs(yoff);
    zoff = fabs(zoff);

    // apply offset signs from string
    xoff *= (xoff_s == '+') ? +1.0 : -1.0;
    yoff *= (yoff_s == '+') ? +1.0 : -1.0;
    zoff *= (zoff_s == '+') ? +1.0 : -1.0;

    if(debug)
        fprintf(stderr,"Reconstruction matrix: %ux%ux%u\n"
                       "Voxel size (mm): %g\n"
                       "FOV center (mm): %+g %+g %+g\n",
                       xres,yres,zres,voxel_size,xoff,yoff,zoff);

    // create volume geometry                                             FOV center (mm)
    CVolumeGeometry3D *vol_geom = createVolGeom(voxel_size,xres,yres,zres,xoff,yoff,zoff);

    // create cone-beam projection geometry
    CConeVecProjectionGeometry3D *proj_geom = createConeProjGeom(cthdr);

    // create CUDA 3D projector
    CCudaProjector3D *proy = create_projector_3d_cuda(proj_geom,vol_geom);

    // create composite geometry manager
    CCompositeGeometryManager *cgeomgr = new CCompositeGeometryManager();

    // time variables
    double t0=0.0,t1=0.0,t2=0.0,dt=0.0;
    
    if(debug) {
        struct timeval tp; gettimeofday(&tp,NULL);
        t0 = tp.tv_sec + 1E-6 * tp.tv_usec; // initial time (seconds)
    }

    /* =============
       EMULATE SIRT!
       ============= */

    // Memory usage is 3x(volume data + projection data), once for input data,
    // another for output data, the third for precomputed weights

    // temporary BP/FP data
    CFloat32ProjectionData3DMemory *projData;
    CFloat32VolumeData3DMemory *tmpData;

    // START precompute weights
    tmpData = create_data_3d_vol(vol_geom,1.0f);
    CFloat32ProjectionData3DMemory *lineWeight = create_data_3d_sino(proj_geom);
    cgeomgr->doFP(proy,tmpData,lineWeight);
    { // precompute compute line weights
        float32 *linwei = lineWeight->getData();
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(size_t ix=0;ix<dataSize(lineWeight);ix++)
            linwei[ix] = linwei[ix] > 0.000001f ? 1 / linwei[ix] : 0.0f;
    }

    projData = create_data_3d_sino(proj_geom,1.0f);
    CFloat32VolumeData3DMemory *pixelWeight = create_data_3d_vol(vol_geom);
    cgeomgr->doBP(proy,pixelWeight,projData);
    { // precompute pixel weights
        float32 *pixwei = pixelWeight->getData();
        const float32 relaxation = 1.99f; // epsilon = 0.005 (Gregor & Benson '08)
#ifdef _OPENMP
#pragma omp parallel for
#endif
        for(size_t ix=0;ix<dataSize(pixelWeight);ix++)
            pixwei[ix] = (pixwei[ix] > 0.000001f ? 1 / pixwei[ix] : 0.0f) * relaxation;
    }
    // END precompute weights

    // START init
    CFloat32ProjectionData3DMemory *sinoData = create_data_3d_sino(proj_geom,sino_data);
    CFloat32VolumeData3DMemory *volumeData = create_data_3d_vol(vol_geom,0.0f);
    delete proj_geom;
    delete vol_geom;
    // END init

    double inorm = 0.0;
    { // compute initial norm
        const float32 *sindat = sinoData->getDataConst();
#ifdef _OPENMP
#pragma omp parallel for reduction(+:inorm)
#endif
        for(size_t ix=0;ix<dataSize(sinoData);ix++)
            inorm += sindat[ix] * sindat[ix];
    }
    inorm = sqrt(inorm);
    
    if(debug) {
        struct timeval tp; gettimeofday(&tp,NULL);
        t1 = tp.tv_sec + 1E-6 * tp.tv_usec; // time at start of iterations (seconds)
        fprintf(stderr,"Iteration started\n");
    }

    // START iterate
    const int pauseiter = 50; // pause every this iterations
    const int pauselen = 300; // pause length in seconds
    for(unsigned int i=0;i<iter;i++) {
        if(debug) fprintf(stderr,"Iteration %d/%d\n",i+1,iter);

        if(i>0) cgeomgr->doFP(proy,volumeData,projData); // forward-project
        else projData->setData(0.0f); // set to zero initially

        { // subtract projection from sinogram and apply line weights
            float32 *prodat = projData->getData();
            const float32 *sindat = sinoData->getDataConst();
            const float32 *linwei= lineWeight->getDataConst();
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t ix=0;ix<dataSize(projData);ix++)
                prodat[ix] = (sindat[ix] - prodat[ix]) * linwei[ix];
        }

        // back-project
        cgeomgr->doBP(proy,tmpData,projData);

        { // apply pixel weights and sum with volume data
            float32 *voldat = volumeData->getData();
            const float32 *tmpdat = tmpData->getDataConst();
            const float32 *pixwei= pixelWeight->getDataConst();
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t ix=0;ix<dataSize(volumeData);ix++)
                voldat[ix] += tmpdat[ix] * pixwei[ix];
        }
        
        // clamp min
        if(clampMin) {
            float32 *voldat = volumeData->getData();
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t ix=0;ix<dataSize(volumeData);ix++)
                if(voldat[ix] < min) voldat[ix] = min;
        }

        // clamp max
        if(clampMax) {
            float32 *voldat = volumeData->getData();
#ifdef _OPENMP
#pragma omp parallel for
#endif
            for(size_t ix=0;ix<dataSize(volumeData);ix++)
                if(voldat[ix] > max) voldat[ix] = max;
        }

        // pause to cool GPU
        if(pause && i + 1 < iter && (i + 1) % pauseiter == 0) {
            if(debug) fprintf(stderr,"%d of %d (max) iterations: idling %ds to cool GPU... ",i+1,iter,pauselen);
            sleep(pause);
            if(debug) fprintf(stderr,"resuming execution\n");
        }
    }
    delete lineWeight;
    delete pixelWeight;
    delete tmpData;

    if(debug) {
        struct timeval tp; gettimeofday(&tp,NULL);
        t2 = tp.tv_sec + 1E-6 * tp.tv_usec; // final time (seconds)
        fprintf(stderr,"Iteration ended\n");
    }

    if(debug) fprintf(stderr,"Computing final norm\n");
    cgeomgr->doFP(proy,volumeData,projData);
    double norm = 0.0;
    { // compute final norm
        const float32 *prodata = projData->getDataConst();
        const float32 *sindata= sinoData->getDataConst();
#ifdef _OPENMP
#pragma omp parallel for reduction(+:norm)
#endif
        for(size_t ix=0;ix<dataSize(projData);ix++)
            norm += (sindata[ix] - prodata[ix]) * (sindata[ix] - prodata[ix]);
    }
    norm = sqrt(norm);

    delete sinoData;
    delete projData;
    delete cgeomgr;
    delete proy;

    if(debug) {
        fprintf(stderr,"Iterations: %d\n",iter);
        fprintf(stderr,"Initial norm of difference: %g\n",inorm);
        fprintf(stderr,"Final norm of difference: %g\n",norm);
        dt = t2 - t0;
        if(dt < 60.0)
            fprintf(stderr,"Total reconstruction time: %gs\n",dt);
        else if(dt < 3600.0)
            fprintf(stderr,"Total reconstruction time: %gm\n",dt/60.0);
        else
            fprintf(stderr,"Total reconstruction time: %gh\n",dt/3600.0);
        dt = (t2 - t1) / iter;
        if(dt < 60.0)
            fprintf(stderr,"Mean time per iteration: %gs\n",dt);
        else
            fprintf(stderr,"Mean time per iteration: %gm\n",dt/60.0);
    }

    // copy reconstructed image
    size_t size = dataSize(volumeData);
    float *result = (float*)malloc(size*sizeof(float));
    memcpy(result,volumeData->getDataConst(),size*sizeof(float));
    delete volumeData;

    if(size_out != NULL) *size_out = size;

    return result;
}

int main (int argc, char *argv[])
{
    if(argc != 6) {
        fprintf(stderr,"[ERROR] Please supply CT header, sinogram, geometry, iterations and output filenames\n");
        exit(EXIT_FAILURE);
    }

    CT_FILE_HEADER cthdr;
    { // load CT header data
        const char *file_cthdr = argv[1];
        if(ctheader_read(file_cthdr,&cthdr)) {
            fprintf(stderr,"[ERROR] Cannot read CT header: %s\n",file_cthdr);
            exit(EXIT_FAILURE);
        }
    }

    size_t size = (size_t)cthdr.NProy * cthdr.Pixels_X * cthdr.Pixels_Z; // sino size
    float *sino_data = (float*)malloc(size*sizeof(float));
    { // load sinogram data (possibly gzip compressed)
        const char *file_sino = argv[2];
        gzFile zfp = gzopen(file_sino,"rb");
        if(gzfread(sino_data,sizeof(float),size,zfp) != size) {
            fprintf(stderr,"[ERROR] Failed reading sinogram: %s\n",file_sino);
            exit(EXIT_FAILURE);
        }
        gzclose(zfp);
    }

    float *reco_img;
    size_t reco_siz;
    { // perform reconstruction
        const char *str_geom = argv[3];
        const char *str_iter = argv[4];
        reco_img = astra_sirt_reconstruction(&cthdr,sino_data,str_geom,atoi(str_iter),
                true,0.0,false,1.0,true,true,&reco_siz);
    }
    if(reco_img == NULL) {
        fprintf(stderr,"[ERROR] Reconstruction failed\n");
        exit(EXIT_FAILURE);
    }

    { // save reconstructed image
        const char *file_out = argv[5];
        FILE *fp = fopen(file_out,"wb");
        if(fwrite(reco_img,sizeof(float),reco_siz,fp) != reco_siz) {
            fprintf(stderr,"[ERROR] Failed saving reconstructed image: %s\n",file_out);
            exit(EXIT_FAILURE);
        }
        fclose(fp);
    }

    return 0;
}
